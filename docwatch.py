#!/usr/bin/env python
"""A small utility to live build and reload in a webbrowser when a source or
config file changes. Very helpful for writing documentation."""

import platform

from livereload import Server, shell
import webbrowser


def main():
    """main routine"""
    make_cmd = 'make html'
    if platform.system() == 'FreeBSD':
        make_cmd = 'gmake html'
    server = Server()
    server.watch('docs/*.rst', shell(make_cmd, cwd='docs'))
    server.watch('dhp/*.py', shell(make_cmd, cwd='docs'))
    server.watch('dhp/VI/*.py', shell(make_cmd, cwd='docs'))
    server.watch('dhp/cache/*.py', shell(make_cmd, cwd='docs'))
    server.watch('dhp/doq/*.py', shell(make_cmd, cwd='docs'))
    server.watch('dhp/math/*.py', shell(make_cmd, cwd='docs'))
    server.watch('dhp/search/*.py', shell(make_cmd, cwd='docs'))
    server.watch('dhp/structures/*.py', shell(make_cmd, cwd='docs'))
    server.watch('dhp/tempus/*.py', shell(make_cmd, cwd='docs'))
    server.watch('dhp/test/*.py', shell(make_cmd, cwd='docs'))
    server.watch('dhp/transforms/*.py', shell(make_cmd, cwd='docs'))
    server.watch('dhp/xml/*.py', shell(make_cmd, cwd='docs'))
    webbrowser.open_new_tab('http://localhost:5500')
    server.serve(root='docs/_build/html', host='0.0.0.0')


if __name__ == '__main__':
    main()
