Dirty Hungarian Phrasebook
==========================
``dhp`` is a library of snippets almost guaranteed to get you into trouble.

I obtained it from a vendor, on the corner, outside of PyCon.

Actually, this is a growing repository of routines that I find helpful from time to time. I think you might too.


Example Phrasebook Contents
---------------------------
See the `Full Documentation <http://dhp.rtfd.org/>`_ for a complete listing.

**dhp.doq**

  * **DOQ** Data Object Query mapper.  Query simple data sources with an ORM like style.

  ::

    from dhp.doq import DOQ

    doq = DOQ(data_objects=data_source)
    for rec in doq.filter(occupation='Publisher'):
      print(rec)


**dhp.structures**

  * **DictDot** - subclasses Python's built-in dict object and offers attribute access to the dictionary.

  ::

    from dhp.structures import DictDot

    my_dict = {'hovercraft': 'eels', 'speed': 42}
    dicdot = DictDot(my_dict)
    assert dicdot.hovercraft == 'eels'
    assert dicdot.speed == 42


**dhp.test**

  * **tempfile_containing** - generate a temporary file that contains indicated contents and returns the filename for use.  When finished the tempfile is removed.

::

    from dhp.test import tempfile_containing

    contents = 'I will not buy this record, it is scratched.'
    with tempfile_containing(contents) as fname:
        do_something(fname)

**dhp.xml**

 * **xml_to_dict** - parse any ugly xml to a python dictionary.

::

    from dhp.xml import xml_to_dict

    xml = '<vehicle type="Hovercraft"><filled/><cargo>eels</cargo></vehicle>'
    xml_to_dict(xml)

    {'vehicle': {'@type':'Hovercraft',
                 'cargo':'eels',
                 'filled': None}
    }


Supports
--------
Tested on Python 2.7, 3.2, 3.3, 3.4

.. image:: https://drone.io/bitbucket.org/dundeemt/dhp/status.png
    :target: https://drone.io/bitbucket.org/dundeemt/dhp/latest
    :alt: Build Status

.. image:: https://readthedocs.org/projects/dhp/badge/?version=v0.0.15
    :target: http://dhp.readthedocs.org/en/v0.0.15/?badge=v0.0.15
    :alt: Documentation Status

Requirements
------------
None.

Installation
------------
Make sure to get the latest version.

  pip install dhp

Documentation
--------------
`Full Docs <http://dhp.rtfd.org/>`_
