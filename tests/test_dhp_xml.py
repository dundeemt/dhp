'''test dhp.xml package - uses py.test'''
# pylint:disable=f0401
import pytest

from dhp.xml import xml_to_dict, dict_to_xml, NoRootExeception, ParseError


def test_xml_empty():
    '''test when input is an empty string'''
    with pytest.raises(ParseError):
        assert xml_to_dict('')


def test_xml_none():
    '''test when input is None'''
    with pytest.raises(TypeError):
        assert xml_to_dict(None)


def test_xml_malformed():
    '''test malformed xml input'''
    with pytest.raises(ParseError):
        assert xml_to_dict('<tag><frob>7</frob><tag>')


def test_xml_simple():
    '''simple xml'''

    xbuf = "<vehicle>Hovercraft</vehicle>"
    assert xml_to_dict(xbuf) == {'vehicle': 'Hovercraft'}


def test_xml_less_simple():
    '''less simple xml'''
    xbuf = "<vehicle><type>Hovercraft</type><cargo>eels</cargo></vehicle>"

    assert xml_to_dict(xbuf) == {'vehicle': {'type': 'Hovercraft',
                                             'cargo': 'eels'}}


def test_xml_with_attrs():
    '''things that makes lots of other xml_to_dicts fail'''
    xbuf = '<vehicle type="Hovercraft"><cargo>eels</cargo></vehicle>'

    assert xml_to_dict(xbuf) == {'vehicle': {'@type': 'Hovercraft',
                                             'cargo': 'eels'}}


def test_xml_with_monos():
    '''things that makes lots of other xml_to_dicts fail'''
    xbuf = '<vehicle type="Hovercraft"><filled/><cargo>eels</cargo></vehicle>'

    assert xml_to_dict(xbuf) == {'vehicle': {'@type': 'Hovercraft',
                                             'cargo': 'eels',
                                             'filled': None}}


def test_xml_with_list():
    '''things that makes lots of other xml_to_dicts fail'''
    xbuf = '''<vehicle type="Hovercraft">
                <filled/>
                <cargo>wet eels</cargo>
                <cargo>slimey eels</cargo>
              </vehicle>'''

    assert xml_to_dict(xbuf) == {'vehicle': {'@type': 'Hovercraft',
                                             'cargo': ['wet eels',
                                                       'slimey eels'],
                                             'filled': None}}


def test_xml_records():
    '''multiple entries'''
    xbuf = '''
    <CATALOG>
    <CD id="1">
        <TITLE>Empire Burlesque</TITLE>
        <ARTIST>Bob Dylan</ARTIST>
        <COUNTRY>USA</COUNTRY>
        <COMPANY>Columbia</COMPANY>
        <PRICE>10.90</PRICE>
        <YEAR>1985</YEAR>
    </CD>
    <CD id="2">
        <TITLE>Hide your heart</TITLE>
        <ARTIST>Bonnie Tyler</ARTIST>
        <COUNTRY>UK</COUNTRY>
        <COMPANY>CBS Records</COMPANY>
        <PRICE>9.90</PRICE>
        <YEAR>1988</YEAR>
    </CD>
    </CATALOG>'''
    expected = {'CATALOG': {'CD': [{'@id': '1',
                                    'ARTIST': 'Bob Dylan',
                                    'COMPANY': 'Columbia',
                                    'COUNTRY': 'USA',
                                    'PRICE': '10.90',
                                    'TITLE': 'Empire Burlesque',
                                    'YEAR': '1985'},
                                   {'@id': '2',
                                    'ARTIST': 'Bonnie Tyler',
                                    'COMPANY': 'CBS Records',
                                    'COUNTRY': 'UK',
                                    'PRICE': '9.90',
                                    'TITLE': 'Hide your heart',
                                    'YEAR': '1988'}]}}
    assert xml_to_dict(xbuf) == expected


# pylint:disable= no-init, old-style-class, no-self-use
class TestDictToXML:
    '''test the dict_to_xml function'''
    def test_simple(self):
        '''classic test'''
        test_dict = {
            'foo': {
                'bar': {
                    'baz': 'what'}
            }
        }
        expect = '<?xml version="1.0" ?><foo><bar><baz>what</baz></bar></foo>'
        assert dict_to_xml(test_dict) == expect

    def test_root_is_not_dict(self):
        '''Since function builds complete xml doc, dictionary structure must
        resemble and xml document meaning the outer dictionary should have a
        single key and dictionary associated with that key.'''
        the_dict = {'vehicle': 'Hovercraft'}
        with pytest.raises(NoRootExeception):
            dict_to_xml(the_dict)

    def test_root_has_multiple_keys(self):
        '''see test_root_is_not_dict'''
        the_dict = {'root': {'foo': 1}, 'bar': 2}
        with pytest.raises(NoRootExeception):
            dict_to_xml(the_dict)

    def test_with_attrs(self):
        '''test that any attrs are associated with the root node.'''
        the_dict = {'root': {'foo': '1'}}
        attrs = {'xml:lang': 'en-US'}
        expected = '<?xml version="1.0" ?><root xml:lang="en-US"><foo>1</foo>'\
                   '</root>'
        assert dict_to_xml(the_dict, attrs) == expected
