'''test dhp.cache utilities'''

from dhp.cache import CacheMiss, CacheKeyUnhashable, SimpleCache

import pytest


# pylint:disable=w0232, c1001, r0201
class TestSimpleCache:
    '''SimpleCache tests.'''
    def test_simplecache_hit(self):
        '''test simple case of cache hit.'''
        scache = SimpleCache()
        scache.put(key='foo', value='bar')
        assert scache.get(key='foo') == 'bar'

    def test_simplecache_miss(self):
        '''test the a cache miss raises CacheMiss'''
        scache = SimpleCache()
        with pytest.raises(CacheMiss):
            scache.get(key='foo')

    def test_simplecache_invalidate(self):
        '''test that invalidate removes the entry'''
        scache = SimpleCache()
        scache.put(key='foo', value='bar')
        scache.invalidate(key='foo')
        with pytest.raises(CacheMiss):
            scache.get(key='foo')

    def test_simplecache_invalidate_bad(self):
        '''attempt to invalidate non-existing key'''
        scache = SimpleCache()
        with pytest.raises(CacheMiss):
            scache.invalidate(key='foo')

    def test_simplecache_stats(self):
        '''test stats proprty of SimpleCache'''
        scache = SimpleCache()
        assert scache.stats['cache_size'] == 0
        scache.put('foo', 'bar')
        _ = scache.get('foo')       # NOQA
        stats = scache.stats
        assert stats['cache_hits'] == 1
        assert stats['cache_misses'] == 1
        scache.invalidate(key='foo')
        assert scache.stats['cache_invalidated'] == 1
        assert len(scache.stats) == 4

    def test_simplecache_unhashable(self):
        '''test exception if key is unhashable.'''
        scache = SimpleCache()
        with pytest.raises(CacheKeyUnhashable):
            scache.put(key=[1, 2, 3], value=4)
        with pytest.raises(CacheKeyUnhashable):
            scache.get(key=[1, 2, 3])
        with pytest.raises(CacheKeyUnhashable):
            scache.invalidate(key=[1, 2, 3])
