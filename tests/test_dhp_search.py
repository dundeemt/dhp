'''test dhp.search package - uses py.test'''

import pytest

from dhp.search import fuzzy_search


def test_documentation():
    '''test the documentation example'''
    haystack = ['.bob', 'bob.', 'bo.b', 'fred']
    assert fuzzy_search('bob', haystack) == ['bob.', '.bob', 'bo.b']


def test_corner():
    '''a corner case that can display ranking problems'''
    corpus = ['api_user.doc', 'user_group.doc']
    assert fuzzy_search('user', corpus) == ['user_group.doc', 'api_user.doc']


def test_basic_search():
    '''test return and ranking'''
    corpus = ['django_migrations.py',
              'django_admin_log.py',
              'main_generator.py',
              'migrations.py',
              'api_user.doc',
              'user_group.doc',
              'accounts.txt']
    assert fuzzy_search('mig', corpus) == ['migrations.py',
                                           'django_migrations.py',
                                           'main_generator.py',
                                           'django_admin_log.py']


def test_single_char_search():
    '''test results of single char searches'''
    corpus = ['django_migrations.py',
              'django_admin_log.py',
              'main_generator.py',
              'migrations.py',
              'api_user.doc',
              'user_group.doc',
              'accounts.txt']
    assert fuzzy_search('m', corpus) == ['main_generator.py',
                                         'migrations.py',
                                         'django_migrations.py',
                                         'django_admin_log.py']


def test_two_char_search():
    '''test result of two char search'''
    corpus = ['django_migrations.py',
              'django_admin_log.py',
              'main_generator.py',
              'migrations.py',
              'api_user.doc',
              'user_group.doc',
              'accounts.txt']
    assert fuzzy_search('mi', corpus) == ['migrations.py',
                                          'django_migrations.py',
                                          'django_admin_log.py',
                                          'main_generator.py']


def test_no_match():
    '''test results of no match searches'''
    corpus = ['django_migrations.py',
              'django_admin_log.py',
              'main_generator.py',
              'migrations.py',
              'api_user.doc',
              'user_group.doc',
              'accounts.txt']
    assert fuzzy_search('xz', corpus) == []
    assert fuzzy_search('wz', corpus) == []
    assert fuzzy_search('w', corpus) == []


@pytest.mark.parametrize('needle,expected', [
    ('bob', ['bob.', '.bob', 'bo.b']),
    ('Bob', ['bob.', '.bob', 'bo.b']),
    ])
def test_needle_mixed_case(needle, expected):
    '''test needle mixed case'''
    haystack = ['.bob', 'bob.', 'bo.b', 'fred']
    assert fuzzy_search(needle, haystack) == expected


def test_haystack_mixed_case():
    '''test haystack with mixed case'''
    haystack = ['80\'s College', 'Alexs Only', 'Classical',
                'Gillians playlist', 'Kool Christmas', 'Soul', 'Workout']
    assert fuzzy_search('g', haystack) == ['Gillians playlist',
                                           '80\'s College']
