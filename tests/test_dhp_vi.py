# -*- coding: utf-8 -*-
'''test objects from the dhp.VI package'''

import sys

from dhp.VI import iteritems
from dhp.VI import py_ver
from dhp.VI import to_unicode, unicode  # pylint:disable=w0622


def test_xml_iteritems():
    '''test the iteritems function'''

    buf = {'foo': 'foo', 'bar': 'bar', 'baz': 4, 'matches': [1, 2, 3]}
    out = {}
    for k, val in iteritems(buf):
        assert k in buf
        assert val in buf.values()
        out[k] = val
    assert out == buf


def test_pyver():
    '''test the PY_VER module variable'''
    assert py_ver() == sys.version_info[0]


def test_to_unicode():
    '''test the to_unicode function'''
    buf = 'café'
    assert isinstance(buf, str)
    assert isinstance(to_unicode(buf), unicode)
