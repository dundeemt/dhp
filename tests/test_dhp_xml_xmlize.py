'''test function based implementation.'''

# import warnings
import pytest

from dhp.xml import obj_to_xml, _is_udo, MissingRequiredException


# pylint:disable=c1001, c0102, w0232, r0903, r0201
class NewStyle(object):     # pylint:disable=r0903
    '''a plain jane new style class def'''
    pass


class OldStyle:
    '''an old style class def'''
    pass


def test_object_new_empty():
    '''test an empty new style object'''
    nstyle = NewStyle()
    assert obj_to_xml(nstyle) == '<NewStyle></NewStyle>'


def test_object_old_empty():
    '''test an empty old style object'''
    nstyle = OldStyle()
    assert obj_to_xml(nstyle) == '<OldStyle></OldStyle>'


def test_dict():
    '''test dict'''
    assert obj_to_xml({'foo': 7}) == '<foo>7</foo>'


def test_dict2():
    '''test dict with 2 items'''
    expected = '<bar>frog</bar><foo>7</foo>'
    assert obj_to_xml({'foo': 7, 'bar': 'frog'}) == expected


def test_object_new_simple():
    '''test a simple new object'''
    nstyle = NewStyle()
    nstyle.foo = 7          # pylint:disable=c0102, w0201
    nstyle.bar = 'frog'     # pylint:disable=c0102, w0201
    expected = "<NewStyle><bar>frog</bar><foo>7</foo></NewStyle>"
    assert obj_to_xml(nstyle) == expected


def test_list_simple():
    '''test a simple list'''
    nstyle = NewStyle()
    nstyle.foo = [7, 5]     # pylint:disable=c0102, w0201
    expected = "<NewStyle><foo><item>7</item><item>5</item></foo></NewStyle>"
    assert obj_to_xml(nstyle) == expected


def test_list_udo():
    '''test a list of user defined objects'''
    class Bar(object):      # pylint:disable=c0111, r0903
        def __init__(self, value):
            self.value = value

    nstyle = NewStyle()
    nstyle.foo = [Bar(7), Bar(5)]   # pylint:disable=w0201
    expected = "<NewStyle><foo><Bar><value>7</value></Bar>"\
               "<Bar><value>5</value></Bar></foo></NewStyle>"
    assert obj_to_xml(nstyle) == expected


class TestIsUDO:
    '''testing the _is_udo function'''
    @pytest.mark.parametrize("obj", [NewStyle(), OldStyle()])
    def test_true(self, obj):
        '''test user defined objects'''
        assert _is_udo(obj)

    @pytest.mark.parametrize("obj", [1, 1.2, "dog", {}, [], set([1, 2])])
    def test_false(self, obj):
        '''test things other than user defined objects.'''
        assert _is_udo(obj) is False


def test_non_data():
    '''test serialzing an object with non-data elements.'''
    class NonSimple(object):
        '''a class to test'''
        def __init__(self, value):
            self.value = value

        def echo(self):
            '''return the value'''
            return self.value

        @property
        def prop(self):
            '''simple property'''
            return self.value

        @staticmethod
        def double(value):
            '''a static method'''
            return 2 * '%s' % value

    expected = "<NonSimple><value>2</value></NonSimple>"
    non = NonSimple(2)
    assert obj_to_xml(non) == expected


class TestRequiredElements:
    '''test the required elements checking/reporting mechanism'''
    def test_not_specified(self):
        '''test when _required_elements is not set'''
        assert NewStyle()

    def test_empty(self):
        '''test when _required_elements is there but empty'''
        class RequiredEls(object):
            '''test required'''
            __required_elements = {}

            def __init__(self, foo):
                self.foo = foo

        required_empty = RequiredEls(foo=7)
        expected = "<RequiredEls><foo>7</foo></RequiredEls>"
        assert expected == obj_to_xml(required_empty)

    def test_all_present(self):
        '''test when all elements specified are present'''
        class RequiredEls(object):
            '''test required'''
            __required_elements = {'foo': 'needed for testing'}

            def __init__(self, foo):
                self.foo = foo

        required_empty = RequiredEls(foo=7)
        expected = "<RequiredEls><foo>7</foo></RequiredEls>"
        assert expected == obj_to_xml(required_empty)

    def test_one_missing(self):
        '''test when one required element is missing'''
        class RequiredEls(object):
            '''test required'''
            __required_elements = {'bar': 'needed for testing'}

            def __init__(self, foo):
                self.foo = foo

        required_empty = RequiredEls(foo=7)
        with pytest.raises(MissingRequiredException):
            obj_to_xml(required_empty)

    def test_multiple_missing(self):
        '''test when multipe required elements are missing'''
        class RequiredEls(object):
            '''test required'''
            __required_elements = {'bar': 'needed for testing',
                                   'foo': 'is found',
                                   'baz': 'is missing'}

            def __init__(self, foo):
                self.foo = foo

        required_empty = RequiredEls(foo=7)
        with pytest.raises(MissingRequiredException):
            obj_to_xml(required_empty)

    def test_present_but_none(self):
        '''test when all elements specified are present but their value is None
        which would exclude them from being output as expected.'''
        class RequiredEls(object):
            '''test required'''
            __required_elements = {'foo': 'needed for testing'}

            def __init__(self, foo):
                self.foo = foo

        required_empty = RequiredEls(foo=None)
        with pytest.raises(MissingRequiredException):
            obj_to_xml(required_empty)


class TestKnownElements:
    '''test the know elements checking/reporting mechanism'''
    # pylint:disable=e1101
    def test_not_specified(self):
        '''test when _known_elments is not set'''
        assert NewStyle()

    def test_empty(self):
        '''test when _known_elements is there but empty'''
        class KnownEls(object):
            '''test required'''
            __known_elements = {}

            def __init__(self, foo):
                self.foo = foo

        known_empty = KnownEls(foo=7)
        expected = "<KnownEls><foo>7</foo></KnownEls>"
        assert expected == obj_to_xml(known_empty)

    def test_all_present(self):
        '''test when all elements specified are present'''
        class KnownEls(object):
            '''test required'''
            __known_elements = {'foo': 'known but not required'}

            def __init__(self, foo):
                self.foo = foo

        known_empty = KnownEls(foo=7)
        expected = "<KnownEls><foo>7</foo></KnownEls>"
        assert expected == obj_to_xml(known_empty)

    def test_one_extra(self):
        '''test when one extra element is present'''
        class KnownEls(object):
            '''test required'''
            __known_elements = {'foo': 'known but not required'}

            def __init__(self, foo):
                self.foo = foo
                self.bar = 7

        known = KnownEls(foo=7)
        with pytest.warns(SyntaxWarning):
            obj_to_xml(known)

    def test_multiple_extra(self):
        '''test when multipe extra elements are present'''
        class KnownEls(object):
            '''test required'''
            __known_elements = {'foo': 'known but not required'}

            def __init__(self, foo):
                self.foo = foo
                self.bar = 7
                self.baz = 'dog'

        known = KnownEls(foo=7)
        with pytest.warns(SyntaxWarning) as records:
            obj_to_xml(known)
            assert records[0].message.args[0] == "unknown: ['bar', 'baz']"


class TestRequiredKnownElements:
    '''test interactions when both required and known are specified.'''
    # test_neither_specified already covered by
    # TestRequiredElments.test_not_specified and
    # TestKnownElements.test_not_specified.

    # pylint:disable=e1101

    def test_present_but_empty(self):
        '''both exist but are empty'''
        class VerifyEls(object):
            '''test required'''
            __required_elements = {}
            __known_elements = {}

            def __init__(self, foo):
                self.foo = foo

        vels = VerifyEls(foo=7)
        expected = "<VerifyEls><foo>7</foo></VerifyEls>"
        assert expected == obj_to_xml(vels)

    def test_required_present(self):
        '''both exist but known empty'''
        class VerifyEls(object):
            '''test required'''
            __required_elements = {'foo': 'a required element'}
            __known_elements = {}

            def __init__(self, foo):
                self.foo = foo

        vels = VerifyEls(foo=7)
        expected = "<VerifyEls><foo>7</foo></VerifyEls>"
        assert expected == obj_to_xml(vels)

    def test_known_present(self):
        '''both both exist but required empty'''
        class VerifyEls(object):
            '''test known'''
            __required_elements = {}
            __known_elements = {'foo': 'a required element'}

            def __init__(self, foo):
                self.foo = foo

        vels = VerifyEls(foo=7)
        expected = "<VerifyEls><foo>7</foo></VerifyEls>"
        assert expected == obj_to_xml(vels)

    def test_all_present(self):
        '''test when both are specified and all is well.'''
        class VerifyEls(object):
            '''test known and required'''
            __required_elements = {'foo': 'a required element'}
            __known_elements = {'bar': 'a known element'}

            def __init__(self, foo):
                self.foo = foo
                self.bar = 'cat'

        vels = VerifyEls(foo=7)
        expected = "<VerifyEls><bar>cat</bar><foo>7</foo></VerifyEls>"
        assert expected == obj_to_xml(vels)

    def test_required_missing(self):
        '''test when both specified but a required is missing'''
        class VerifyEls(object):
            '''test known and required'''
            __required_elements = {'foo': 'a required element',
                                   'baz': 'another required'}
            __known_elements = {'bar': 'a known element'}

            def __init__(self, foo):
                self.foo = foo
                self.bar = 'cat'

        vels = VerifyEls(foo=7)
        with pytest.raises(MissingRequiredException):
            obj_to_xml(vels)

    def test_unknown_present(self):
        '''test both spec'd but an unknown is present'''
        class VerifyEls(object):
            '''test known and required'''
            __required_elements = {'foo': 'a required element'}
            __known_elements = {'bar': 'a known element'}

            def __init__(self, foo):
                self.foo = foo
                self.bar = 'cat'
                self.baz = 'dog'

        vels = VerifyEls(foo=7)
        with pytest.warns(SyntaxWarning):
            obj_to_xml(vels)

    def test_both_fail(self):
        '''test both spec'd and a required is missing and an unknown is
        present'''
        class VerifyEls(object):
            '''test known and required'''
            __required_elements = {'foo': 'a required element',
                                   'baz': 'another required'}
            __known_elements = {'bar': 'a known element'}

            def __init__(self, foo):
                self.foo = foo
                self.bar = 'cat'
                self.vector = 9

        vels = VerifyEls(foo=7)
        with pytest.raises(MissingRequiredException):
            obj_to_xml(vels)
