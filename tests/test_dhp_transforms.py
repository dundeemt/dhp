'''test the dhp.transform library'''


from dhp.transforms import to_snake, int2word, chunk_r, filter_dict

import pytest


@pytest.mark.parametrize("val,expected", [
    ("camelCase", "camel_case"),
    ("BjorkedCamelCase", "bjorked_camel_case"),
    ("camel2Case2", "camel2_case2"),
    ("camel22Case", 'camel22_case'),
    ("camelHTTPRequest", "camel_http_request"),
    ("_camelCase", "_camel_case"),
    ("_CamelCase", "_camel_case"),
    ("HTTPResponseCodeXYZ", "http_response_code_xyz"),
])
def test_to_snake(val, expected):
    '''test transformations'''
    assert to_snake(val) == expected


@pytest.mark.parametrize("val,csize,expected", [
    ('abcdefg', 3, ['a', 'bcd', 'efg']),
    ('abcdefg', 2, ['a', 'bc', 'de', 'fg']),
    (23, 3, ['23'])
])
def test_chunk_r(val, csize, expected):
    '''test chunking from the right'''
    hold = val
    assert chunk_r(val, chunk_size=csize) == expected
    assert hold == val


@pytest.mark.parametrize("val,expected", [
    (0, "Zero"),
    (1, "One"),
    (21, "Twenty One"),
    (99, "Ninety Nine"),
    (100, "One Hundred"),
    (121, "One Hundred Twenty One"),
    (299, "Two Hundred Ninety Nine"),
    (100012, "One Hundred Thousand Twelve"),
    (1100012, "One Million One Hundred Thousand Twelve"),
    (99999999999, "Ninety Nine Billion Nine Hundred Ninety Nine Million Nine"
     " Hundred Ninety Nine Thousand Nine Hundred Ninety Nine"),
    (1000000000, 'One Billion'),
    (1000000001, 'One Billion One'),
])
def test_int2word(val, expected):
    '''test transforming an integer value to a word'''
    assert int2word(val) == expected
    assert int2word(str(val)) == expected


@pytest.mark.parametrize("original,keys,expected", [
    ({}, ['a', 'b', 'c'], {}),
    ({'d': 4}, ['a', 'b', 'c'], {}),
    ({'d': 4, 'c': 3}, ['a', 'b', 'c'], {'c': 3}),
    ({'d': 4, 'c': 3, 'b': 2, 'a': 1}, ['a', 'b', 'c'],
     {'a': 1, 'b': 2, 'c': 3}),
])
def test_filter_dict(original, keys, expected):
    '''filter_dict tests'''
    assert filter_dict(original, keys) == expected
