'''test dhp.test package - uses py.test'''

import os

from dhp.test import tempfile_containing


def test_tfc_write():
    '''test tempfile_containing writes contents as expected'''

    with tempfile_containing('foo') as fname:
        assert open(fname).read() == 'foo'


def test_tfc_creates():
    '''test tempfile_containing creates file as expected'''

    with tempfile_containing('foo') as fname:
        assert os.path.exists(fname)


def test_tfc_removes():
    '''test tempfile_containing removes file as expected'''

    with tempfile_containing('foo') as fname:
        pass
    assert os.path.exists(fname) is False


def test_tfc_stackability():
    '''test tempfile_containing operates as expected when stacked'''

    with tempfile_containing('foo') as fname1:
        with tempfile_containing('bar') as fname2:
            with tempfile_containing('baz') as fname3:
                assert fname1 != fname2
                assert fname1 != fname3
                assert fname2 != fname3

                assert open(fname1).read() == 'foo'
                assert open(fname2).read() == 'bar'
                assert open(fname3).read() == 'baz'

    assert os.path.exists(fname1) is False
    assert os.path.exists(fname2) is False
    assert os.path.exists(fname3) is False
