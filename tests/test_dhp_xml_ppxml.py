'''test dhp.xml package - uses py.test'''

#
from dhp.xml import ppxml


def test_xml_simple():
    '''simple xml'''

    xbuf = "<vehicle>Hovercraft</vehicle>"
    out = ['<?xml version="1.0" ?>',
           xbuf,
           '']
    assert ppxml(xbuf) == '\n'.join(out)


def test_xml_less_simple():
    '''less simple xml'''
    xbuf = '<vehicle><type>Hovercraft</type><cargo>eels</cargo></vehicle>'
    out = ['<?xml version="1.0" ?>',
           '<vehicle>',
           '  <type>Hovercraft</type>',
           '  <cargo>eels</cargo>',
           '</vehicle>',
           '']

    assert ppxml(xbuf) == '\n'.join(out)


def test_ppxml_with_attrs():
    '''things that makes lots of other xml_to_dicts fail'''
    xbuf = '<vehicle type="Hovercraft"><cargo>eels</cargo></vehicle>'
    out = ['<?xml version="1.0" ?>',
           '<vehicle type="Hovercraft">',
           '  <cargo>eels</cargo>',
           '</vehicle>',
           '']

    assert ppxml(xbuf) == '\n'.join(out)


def test_ppxml_with_monos():
    '''things that makes lots of other xml_to_dicts fail'''
    xbuf = '<vehicle type="Hovercraft"><filled/><cargo>eels</cargo></vehicle>'

    out = ['<?xml version="1.0" ?>',
           '<vehicle type="Hovercraft">',
           '  <filled/>',
           '  <cargo>eels</cargo>',
           '</vehicle>',
           '']

    assert ppxml(xbuf) == '\n'.join(out)


def test_ppxml_with_list():
    '''things that makes lots of other xml_to_dicts fail'''
    xbuf = '''<vehicle type="Hovercraft">
                <filled/>
                <cargo>wet eels</cargo>
                <cargo>slimey eels</cargo>
              </vehicle>'''

    out = ['<?xml version="1.0" ?>',
           '<vehicle type="Hovercraft">',
           '  <filled/>',
           '  <cargo>wet eels</cargo>',
           '  <cargo>slimey eels</cargo>',
           '</vehicle>',
           '']

    assert ppxml(xbuf) == '\n'.join(out)
