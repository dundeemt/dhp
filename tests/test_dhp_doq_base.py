'''tests for the basic DOQu base class.'''

from dhp.doq import DOQ, DoesNotExist, MultipleObjectsReturned
# from dhp.structures import DictDot


import pytest


class Simple(object):       # pylint:disable=r0903
    '''simple object for testing.'''
    def __init__(self, emp_id, name, dept, hired):
        self.emp_id = emp_id
        self.name = name
        self.dept = dept
        self.hired = hired

    def __repr__(self):
        return "<{0}: {1} - {2}>".format(self.emp_id, self.dept, self.name)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


def test_data():
    '''wrap in a function to make it more immutable'''
    return [Simple(0, 'Bob', 'IT', '2001-12-22'),
            Simple(1, 'Alice', 'IT', '2002-02-19'),
            Simple(2, 'Rachel', 'MKTG', '2002-02-20'),
            Simple(3, 'Kim', 'QA', '2002-03-15'),
            Simple(5, 'Nick', 'QA', '2003-03-02'),
            Simple(32, 'Hank', 'HR', '2005-04-14')]


# pylint: disable=W0621
@pytest.yield_fixture
def doq():
    ''' yield a DOQ hooked up to the test_data()'''
    yield DOQ(data_objects=test_data())


def test_filter_multi_attr(doq):
    '''test that filtering on multiple attributes works as intended.'''
    rslt = doq.filter(emp_id__gt=4, dept='QA')
    assert rslt.count == 1
    assert rslt[0] == test_data()[4]
    rslt2 = doq.filter(emp_id__gt=4).filter(dept='QA')
    assert rslt2.count == 1
    assert rslt2[0] == rslt[0]
    rslt3 = doq.filter(emp_id__gt=4)
    rslt3 = rslt3.filter(dept='QA')
    assert rslt3.count == 1
    assert rslt3[0] == rslt[0]
    rslt = doq.filter(emp_id__lt=32, hired__gt='2002-01-01',
                      name__icontains='i', dept='IT')
    assert rslt.count == 1
    assert rslt[0] == test_data()[1]


def test_instantiation(doq):
    """ Assert we can instantiate. """
    assert doq


def test_stability(doq):
    '''Test that data_object order is not disturbed unless intended.'''
    assert list(doq) == test_data()
    assert list(doq.all()) == test_data()
    assert list(doq.all().order_by('name').order_by()) == test_data()
    assert list(doq.order_by('name').order_by().all()) == test_data()


# Test Filter Operators:
def test_implied_exact(doq):
    """ can we imply equality """
    rslt = doq.filter(emp_id=0)
    assert len(rslt) == 1
    assert rslt[0] == test_data()[0]


def test_explicit_exact(doq):
    """ can we explicitly call equality """
    rslt = doq.filter(emp_id__exact=0)
    assert len(rslt) == 1
    assert rslt[0] == test_data()[0]


def test_iexact(doq):
    """ Test iexact """
    rslt = list(doq.filter(name__iexact='hank'))
    assert len(rslt) == 1
    assert rslt[0] == test_data()[5]


def test_gt(doq):
    """ Test > """
    rslt = doq.filter(hired__gt='2002-01-01')
    assert len(rslt) == 5
    assert list(rslt) == test_data()[1:]


def test_gte(doq):
    """ Test >= """
    rslt = doq.filter(emp_id__gte=5)
    assert len(rslt) == 2
    assert list(rslt) == test_data()[4:]


def test_lt(doq):
    """ Test < """
    rslt = doq.filter(emp_id__lt=3)
    assert len(rslt) == 3
    assert list(rslt) == test_data()[:3]


def test_lte(doq):
    """ Test <= """
    rslt = doq.filter(emp_id__lte=3)
    assert len(rslt) == 4
    assert list(rslt) == test_data()[:4]


def test_multi_filter2(doq):
    """ Test multiple filters, single call."""
    rslt = doq.filter(emp_id__lte=2, name='Bob')
    assert len(rslt) == 1


def test_chaining_2(doq):
    """Test multiple filters chained together."""
    doq2 = doq.filter(emp_id__lte=2)
    doq2 = doq2.filter(name='Bob')
    assert list(doq2) == list(doq.filter(emp_id__lte=2, name='Bob'))


def test_multi_filter3(doq):
    """ Test <= """
    rslt = doq.filter(emp_id__lte=5, name__icontains='a', dept='IT')
    assert len(rslt) == 1


def test_chaining_3(doq):
    """Test a triple chain."""
    doq3 = doq.all()
    doq3 = doq3.filter(emp_id__lte=5)
    doq3 = doq3.filter(name__icontains='a')
    doq3 = doq3.filter(dept='IT')
    rslt = list(doq.filter(emp_id__lte=5, name__icontains='a',
                           dept='IT'))
    assert list(doq3) == rslt


def test_contains(doq):
    """ test contains """
    rslt = doq.filter(name__contains='A')
    assert len(rslt) == 1


def test_icontains(doq):
    """ test icontains """
    rslt = doq.filter(name__icontains='A')
    assert len(rslt) == 3


def test_in(doq):
    """ Test in """
    rslt = doq.filter(emp_id__in=[1, 2, 3])
    assert len(rslt) == 3


def test_startswith(doq):
    """ Test startswith """
    rslt = doq.filter(name__startswith='Al')
    assert len(rslt) == 1
    assert list(rslt)[0] == test_data()[1]


def test_istartswith(doq):
    """ Test case insensitive startswith """
    rslt = doq.filter(name__istartswith='aL')
    assert len(rslt) == 1
    assert list(rslt)[0] == test_data()[1]


def test_endswith(doq):
    """ Test endswith """
    rslt = doq.filter(name__endswith='k')
    assert len(rslt) == 2
    assert list(rslt) == test_data()[-2:]


def test_iendswith(doq):
    """ Test case insensitive endswith """
    rslt = doq.filter(name__iendswith='K')
    assert len(rslt) == 2
    assert list(rslt) == test_data()[-2:]


def test_range(doq):
    """Test range=(a, b) operator is equivalent to gte=a AND lte=b"""
    r_rslt = doq.filter(emp_id__range=(2, 5))
    gele_rslt = doq.filter(emp_id__gte=2, emp_id__lte=5)
    assert list(r_rslt) == list(gele_rslt)


def test_filter_all(doq):
    """ Return All SonosPlaylists """
    assert len(doq.filter()) == 6


def test_bad_attrname(doq):
    ''' when given a bad attr_name raises '''
    with pytest.raises(AttributeError):
        list(doq.filter(fred='Munster'))


def test_0_found(doq):
    '''test returing an empty'''
    assert list(doq.filter(name='FooBar')) == []


def test_get_good(doq):
    ''' Test a valid get request. '''
    assert doq.get(name='Kim')


def test_get_does_not_exist(doq):
    ''' Test a get with 0 matches. '''
    with pytest.raises(DoesNotExist):
        doq.get(name='FooBar')


def test_get_multiple_returned(doq):
    ''' test a get failure from too many objects matching. '''
    with pytest.raises(MultipleObjectsReturned):
        doq.get(dept='IT')


def test_all(doq):
    ''' test all method. '''
    assert list(doq.all()) == list(doq.filter())


def test_reevals(doq):
    ''' Test that each get/filter gets fresh data. '''
    assert doq.get(emp_id=1)
    assert doq.get(emp_id=2)


def test_repr_truncated(doq):
    '''test the repr function with more than max_items'''
    msg = "<0: IT - Bob>,<1: IT - Alice>,<2: MKTG - Rachel>"\
          "...(remaining elements truncated)..."
    assert msg == str(doq)


def test_repr(doq):
    '''test repr with less than max items.'''
    msg = "<0: IT - Bob>,<1: IT - Alice>,<2: MKTG - Rachel>"
    assert str(doq.filter(emp_id__lt=3)) == msg


def test_orderby_single(doq):
    '''test order_by with a single attr.'''
    expected = ['Alice', 'Bob', 'Rachel']
    ordered = [x.name for x in doq.filter(emp_id__lt=3).order_by('name')]
    assert expected == ordered


def test_orderby_single_desc(doq):
    '''test order_by with a single attr in descending order.'''
    expected = ['Rachel', 'Bob', 'Alice']
    ordered = [x.name for x in doq.filter(emp_id__lt=3).order_by('-name')]
    assert expected == ordered


def test_orderby_itemid_all(doq):
    '''test order_by item_id with SQ:3 and SQ:34.'''
    expected = ['Alice', 'Bob', 'Hank', 'Kim', 'Nick', 'Rachel']
    ordered = [x.name for x in doq.all().order_by('name')]
    assert expected == ordered


def test_orderby_replaces_previous(doq):
    '''Test that assigning a new order_by replaces old.'''
    expected = [x.emp_id for x in doq.order_by('dept')]
    ordered = doq.all().order_by('-name')
    ordered = [x.emp_id for x in ordered.order_by('dept')]
    # expected = [32, 0, 1, 2, 3, 5]
    assert ordered == expected


def test_orderby_clones(doq):
    '''Test that order_by is cloned.'''
    expected = ['Alice', 'Bob', 'Hank', 'Kim', 'Nick', 'Rachel']
    ordered = [x.name for x in doq.order_by('name').all()]
    assert expected == ordered


def test_orderby_multi_attr(doq):
    '''Test order_by w/ multiple attributes'''
    ordered = [x.emp_id for x in doq.all().order_by('-dept', 'name')]
    expected = [3, 5, 2, 1, 0, 32]
    assert ordered == expected


def test_orderby_bad_attrname(doq):
    '''Test passing a bad attrname to order_by'''
    with pytest.raises(AttributeError):
        list(doq.all().order_by('fred'))
    with pytest.raises(AttributeError):
        list(doq.all().order_by('-fred'))
    with pytest.raises(AttributeError):
        list(doq.all().order_by('emp_id', 'fred'))


def test_orderby_random(doq):
    '''Test the abilit to order_by random.'''
    assert list(doq.all()) != list(doq.all().order_by('?'))
    assert list(doq.all().order_by('?')) != list(doq.all().order_by('?'))


def test_exclude_simple(doq):
    '''Test a simple exclusion'''
    result = doq.exclude(emp_id=0)
    assert len(result) == 5
    assert 0 not in [x.emp_id for x in result]


def test_exclude_multiple(doq):
    '''Test an exclusion the excludes many.'''
    result = doq.exclude(emp_id__lt=99)
    assert len(result) == 0


def test_chained_filter_exclude(doq):
    '''Test a filter and exclude chained together.'''
    result = doq.filter(emp_id__gte=5).exclude(emp_id=32)
    assert len(result) == 1
    assert set([x.emp_id for x in result]) == set([5, ])


def test_count(doq):
    '''Test the count property'''
    result = doq.exclude(emp_id=0)
    assert result.count == 5
    assert result.count == len(result)
