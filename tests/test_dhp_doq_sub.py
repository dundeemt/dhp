'''Test subclass'ability of DOQ'''

from dhp.doq import DOQ

import datetime


# Three methods to handle less than stellar data.
#  Method 1: transform the data in the object. (see self.hired)
#  Method 2: Create a shadow attribute and use that. (see self._emp_id)

class Simple(object):       # pylint:disable=r0903
    '''simple object for testing.'''
    def __init__(self, emp_id, name, dept, hired):
        self.emp_id = emp_id
        self._emp_id = int(emp_id.split(':')[1])
        self.name = name
        self.dept = dept
        self.hired = iso2date(hired)    # make proper python date out of data

    def __repr__(self):
        return "<{0}: {1} - {2}>".format(self.emp_id, self.dept, self.name)


def iso2date(date_str):
    '''convert an iso date string to a python date object.'''
    return datetime.datetime.strptime(date_str, "%Y-%m-%d").date()


TEST_DATA = [Simple('E:0', 'Bob', 'IT', '2001-12-22'),
             Simple('E:1', 'Alice', 'IT', '2002-02-19'),
             Simple('E:2', 'Rachel', 'MKTG', '2002-02-20'),
             Simple('E:3', 'Kim', 'QA', '2002-03-15'),
             Simple('E:5', 'Nick', 'QA', '2003-03-02'),
             Simple('E:32', 'Hank', 'HR', '2005-04-14')]


#  Method #3 - subclass DOQ and tell it how you want the data transformed
#              when you sort it.
class FunkyDataQuery(DOQ):
    '''subclass DOQ into a Funky Query Mapper'''

    @staticmethod
    def order_by_key_fn(attrname):
        '''override the base class' so we can transform a given attribute
        in a manner that will ouput the proper order_by result'''
        # emp_id is a string with E:x, we want to order on emp_id with it
        # sorting in numeric, not text order. i.e.
        #       E:2, E:3, E:5, E:32 vs E:2, E:3, E:32, E:5
        if attrname == 'emp_id':
            def key_fn(obj):
                '''return the numeric portion as an integer'''
                return int(DOQ.get_attr(obj, attrname).split(':')[1])
        else:
            def key_fn(obj):
                '''return the standard function.'''
                return DOQ.get_attr(obj, attrname)
        return key_fn


def test_sorting_str_vs_int():
    '''Test str sort vs int.'''
    doq = DOQ(data_objects=TEST_DATA)
    recs = doq.all()
    assert list(recs.order_by('emp_id')) != list(recs.order_by('_emp_id'))

    fdq = FunkyDataQuery(data_objects=TEST_DATA)
    assert list(fdq.all().order_by('emp_id')) == list(recs.order_by(
        '_emp_id'))
