'''test DOQ usage with objects created on the fly from common sources.'''
from dhp.doq import DOQ
from dhp.structures import DictDot
from dhp.VI import StringIO


from collections import namedtuple
import csv
import os


TEST_FILE = StringIO('''emp_id,name,dept,hired
0, "Bob", "IT", "2001-12-22"
1, "Alice", "IT", "2002-02-19"
2, "Rachel", "MKTG", "2002-02-20"
3, "Kim", "QA", "2002-03-15"
5, "Nick", "QA", "2003-03-02"
32, "Hank", "HR", "2005-04-14"''')


def csvdict():
    '''csv dict reader test.'''
    TEST_FILE.seek(0)
    reader = csv.DictReader(TEST_FILE)
    for row in reader:
        yield DictDot(row)


def test_csv_dictdot():
    '''Test order_by w/ multiple attributes on csv data'''
    doq = DOQ(data_objects=csvdict())
    ordered = [x.emp_id for x in doq.all().order_by('-dept', 'name')]
    expected = ['3', '5', '2', '1', '0', '32']
    assert ordered == expected


EmployeeRecord = namedtuple('EmployeeRecord', 'emp_id, name, dept, hired')


def csvtuple():
    '''csv named tuple emitter.'''
    TEST_FILE.seek(0)
    reader = csv.reader(TEST_FILE)
    next(reader)    # skip headers
    for emp in map(EmployeeRecord._make, reader):   # pylint:disable=w0141
        yield emp


def test_namedtuple():
    '''Test order_by w/ multiple attributes on namedtuple data'''
    doq = DOQ(data_objects=csvtuple())
    ordered = [x.emp_id for x in doq.all().order_by('-dept', 'name')]
    expected = ['3', '5', '2', '1', '0', '32']
    assert ordered == expected


def compound_objects():
    '''return a list of data objects that holds subobjects'''
    pth = os.path.dirname(os.path.abspath(__file__))
    jfile = os.path.join(pth, 'users.json')
    with open(jfile, 'r') as hjfile:
        jdata = hjfile.read()
    import json
    return [DictDot(x) for x in json.loads(jdata)]


def test_nested_filter():
    '''Test filtering on nested subobjects'''
    doq = DOQ(data_objects=compound_objects())
    assert doq.count == 10
    rec = doq.get(address__city='Gwenborough')
    assert rec.address.city == 'Gwenborough'
    assert doq.filter(address__geo__lat__gt=0).count == 3
    assert doq.filter(address__geo__lat__lte=0).count == 7


def test_nested_orderby():
    '''Test orderby on nested attributes.'''
    doq = DOQ(data_objects=compound_objects())
    by_lat = [rec.address.geo.lat
              for rec in doq.all().order_by('address__geo__lat')]
    assert by_lat == [-71.4197, -68.6102, -43.9509, -38.2386, -37.3159,
                      -31.8129, -14.399, 24.6463, 24.8918, 29.4572]


def test_nested_exclude():
    '''Test excluding on nested subobject attributes.'''
    doq = DOQ(data_objects=compound_objects())
    assert doq.exclude(address__city='Gwenborough').count == 9
    lte_zero = doq.filter(address__geo__lat__lte=0)
    assert list(doq.exclude(address__geo__lat__gt=0)) == list(lte_zero)


def test_nested_lookups():
    '''Test lookups on nested subobject attributes.'''
    doq = DOQ(data_objects=compound_objects())
    assert doq.filter(address__suite__startswith='Apt.').count == 3
