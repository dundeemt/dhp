"""Test the dhp.structures.ComparableMixin """
import pytest

from dhp.structures import ComparableMixin
from dhp.VI import py_ver


# pylint:disable=c0102
class Foo(ComparableMixin):     # pylint:disable=r0903
    """a class with the mixin"""
    def __init__(self, val):
        self.val = val

    def _cmpkey(self):
        """return the thing to compare for this object."""
        return (self.val, )


class Bar(object):      # pylint:disable=r0903
    """a class w/o the mixin"""
    def __init__(self, val):
        self.val = val


def test_instantiation():
    """test we can instantiate Foo"""
    foo = Foo(3)
    assert foo


def test_equal():
    """test =="""
    fa3 = Foo(3)
    fb3 = Foo(3)
    assert fa3 == fb3
    assert fa3 <= fb3
    assert fa3 >= fb3


def test_dissimiliar():
    """test against different kind of object"""
    fa3 = Foo(3)
    bar = Bar(3)
    if py_ver() == 3:
        with pytest.raises(TypeError):
            assert fa3 > bar
    else:
        assert fa3 > bar


def test_sortable():
    """a test of being sortable"""
    expected = [Foo(1), Foo(2), Foo(3)]
    vals = [Foo(3), Foo(1), Foo(2)]
    assert sorted(vals) == expected


def test_not_sortable():
    """a test of being sortable"""
    expected = [Bar(1), Bar(2), Bar(3)]
    vals = [Bar(3), Bar(1), Bar(2)]
    if py_ver() == 3:
        with pytest.raises(TypeError):
            assert sorted(vals) != expected
    else:
        assert sorted(vals) != expected
