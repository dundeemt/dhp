'''tests for the dhp.tempus module'''

from datetime import timedelta
import pytest

from dhp.tempus import delta_from_interval, IntervalError, interval_from_delta


TEST_CASES = [
    ('8s', {'seconds': 8}),                     # standalone seconds
    ('15m', {'minutes': 15}),                   # standalone minutes
    ('3h', {'hours': 3}),                       # standalone hours
    ('1d', {'days': 1}),                        # standalone days
    ('1w', {'weeks': 1}),                       # standalone weeks
    ('3m4s', {'minutes': 3, 'seconds': 4}),     # minutes/seconds
    ('3h4s', {'hours': 3, 'seconds': 4}),       # hours/seconds
    ('3d4s', {'days': 3, 'seconds': 4}),        # days/seconds
    ('3w4s', {'weeks': 3, 'seconds': 4}),       # weeks/seconds
    ('2h3m4s', {'hours': 2, 'minutes': 3,
                'seconds': 4}),                 # hours/minutes/seconds
    ('2d3m4s', {'days': 2, 'minutes': 3,
                'seconds': 4}),                 # days/minutes/seconds
    ('2w3m4s', {'weeks': 2, 'minutes': 3,
                'seconds': 4}),                 # weeks/minutes/seconds
    ('1d2h3m4s', {'days': 1, 'hours': 2,
                  'minutes': 3, 'seconds': 4}),  # days/hrs/mins/secs
    ('1w2h3m4s', {'weeks': 1, 'hours': 2,
                  'minutes': 3, 'seconds': 4}),  # wks/hrs/mins/secs
    ('1w2d3h4m5s', {'weeks': 1, 'days': 2,
                    'hours': 3, 'minutes': 4,
                    'seconds': 5}),             # wks/days/hrs/mins/secs
    ('1w2d3h4m', {'weeks': 1, 'days': 2,
                  'hours': 3, 'minutes': 4}),   # wks/days/hrs/mins
    ('1w2d3h', {'weeks': 1, 'days': 2,
                'hours': 3}),                   # wks/days/hrs
    ('1w2d3m', {'weeks': 1, 'days': 2,
                'minutes': 3}),                 # wks/days/mins
    ('1w2h3m', {'weeks': 1, 'hours': 2,
                'minutes': 3}),                 # wks/hrs/mins
    ('1w2d', {'weeks': 1, 'days': 2}),          # wks/days
    ('1w2h', {'weeks': 1, 'hours': 2}),         # wks/hrs
    ('1w2m', {'weeks': 1, 'minutes': 2}),       # wks/mins
    ('1d2h3m', {'days': 1, 'hours': 2,
                'minutes': 3}),                 # days/hrs/mins
    ('1d2m', {'days': 1, 'minutes': 2}),        # days/mins
    ('1d2h', {'days': 1, 'hours': 2}),          # days/hrs
    ('', {'seconds': 0}),                       # an empty interval
]


# pylint:disable=w0232,c1001,r0201
class TestDeltaFromInterval:
    '''test the delta_from_interval function.'''

    @pytest.mark.parametrize("bad", [
        ('12'),         # test when no subinterval indicators present
        ('1w3x'),       # unknown subinterval
        ('3d1w2m'),     # subintervals out of order
        ('-1d'),        # negative interval
    ])
    def test_bad_subintervals(self, bad):
        '''problematic intervals'''
        with pytest.raises(IntervalError):
            delta_from_interval(bad)

    @pytest.mark.parametrize("interval, tdelta", TEST_CASES)
    def test_delta_from_interval(self, interval, tdelta):
        '''test interval -> delta'''
        assert delta_from_interval(interval) == timedelta(**tdelta)

    def test_uppercase_periods(self):
        '''test input can use upper case period indicators'''
        interval, tdelta = ('1D2h', {'days': 1, 'hours': 2})
        assert delta_from_interval(interval) == timedelta(**tdelta)


# pylint:disable=r0903
class TestIntervalFromDelta:
    '''test the delta_from_interval function'''

    @pytest.mark.parametrize("interval, tdelta", TEST_CASES)
    def test_delta_from_interval(self, interval, tdelta):
        '''test interval -> delta'''
        assert interval_from_delta(timedelta(**tdelta)) == interval


@pytest.mark.parametrize("interval, tdelta", TEST_CASES)
def test_to_fro(interval, tdelta):
    '''test roundtripping'''
    assert interval_from_delta(delta_from_interval(interval)) == interval
    tdlta = timedelta(**tdelta)
    assert delta_from_interval(interval_from_delta(tdlta)) == tdlta
