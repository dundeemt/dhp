'''test the README.rst for restructuredtext errors'''

import subprocess


def test_readme():
    '''test the readme for restructuredtext errors'''
    # pylint:disable=w0106
    subprocess.check_output(['python', 'setup.py', 'check',
                             '--restructuredtext', '-s']) == ''
