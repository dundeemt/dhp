"""test structures"""
import pytest

from dhp.structures import DictDot


@pytest.mark.parametrize('param,expected', [
    ({'foo': 'bar'}, str),
    ({'foo': 0}, int),
    ({'foo': 1.0}, float),
    ({'foo': ['bar']}, list),
    ({'foo': {'bar': 5}}, DictDot),
    ])
def test_simple(param, expected):
    '''test simple dict'''
    dicdot = DictDot(param)
    assert dicdot.foo == dicdot['foo']
    assert isinstance(dicdot.foo, expected)


def test_empty_dot():
    '''test build from empty adding with dots'''
    dicdot = DictDot()
    dicdot.foo = 5
    assert dicdot['foo'] == 5


def test_empty_key():
    '''test build from empty using a key'''
    dicdot = DictDot()
    dicdot['foo'] = 5
    assert dicdot.foo == 5


def test_nested():
    '''test nested'''
    dicdot = DictDot({
        'foo': {
            'bar': {
                'baz': 'hovercraft',
                'x': 'eels'
            }
        }
    })
    assert dicdot.foo.bar.baz == 'hovercraft'       # pylint:disable=e1101
    assert dicdot['foo'].bar.x == 'eels'
    assert dicdot.foo['bar'].baz == 'hovercraft'


def test_kwargs():
    '''test keyword args'''
    dicdot = DictDot(foo=5)
    assert dicdot.foo == 5
    dicdot = DictDot(foo={'bar': 5})
    assert dicdot.foo.bar == 5      # pylint:disable=e1101
    assert isinstance(dicdot.foo, DictDot)
    assert DictDot(foo=5, bar=2).bar == 2


def test_with_kwargs():
    '''test init with a dict and keyword args'''
    dicdot = DictDot({'foo': 5}, bar=2)
    assert dicdot.foo == 5
    assert dicdot.bar == 2


def test_in():
    '''test with the in keyword'''
    dicdot = DictDot({'foo': 5}, bar=2)
    assert 'foo' in dicdot
    assert 'bar' in dicdot
    assert 'baz' not in dicdot


def test_nonexisting_attr():
    '''test AttributeError raised when accessing missing attribute'''
    dicdot = DictDot(foo=5)
    with pytest.raises(AttributeError):
        _ = dicdot.bar      # NOQA


def test_nonexisting_key():
    '''test KeyError raised when accessing missing key'''
    dicdot = DictDot(foo=5)
    with pytest.raises(KeyError):
        _ = dicdot['bar']       # NOQA


def test_assign_dict():
    '''test that assigning a dict results in a DictDot'''
    dicdot = DictDot()
    dicdot.foo = {'bar': 5}
    assert isinstance(dicdot.foo, DictDot)
    assert dicdot.foo.bar == 5      # pylint:disable=e1101


def test_dicdot_keys():
    '''assert .keys works as expected'''
    dicdot = DictDot({'foo': 5, 'bar': 'open'})
    assert set(dicdot.keys()) == set(['foo', 'bar'])


def test_dicdot_values():
    '''assert .keys works as expected'''
    dicdot = DictDot({'foo': 5, 'bar': 'open'})
    assert set(dicdot.values()) == set([5, 'open'])


def test_delete_dot():
    '''can delete by dot notation'''
    dicdot = DictDot({'foo': 5, 'bar': 'open'})
    del dicdot.foo
    assert len(dicdot) == 1
    assert dicdot.bar == 'open'
    with pytest.raises(AttributeError):
        _ = dicdot.foo      # NOQA


def test_delete_key():
    '''can delete by key'''
    dicdot = DictDot({'foo': 5, 'bar': 'open'})
    del dicdot['foo']
    assert dicdot.bar == 'open'
    with pytest.raises(KeyError):
        _ = dicdot['foo']       # NOQA


def test_subclass():
    '''test that DictDot plays well with others'''
    class Fred(DictDot):
        '''subclass DictDot'''
        pass

    fred = Fred(foo=5, bar={'baz': 'eel'})
    assert fred.foo == 5
    assert isinstance(fred.bar, Fred)


# def test_set_nonexisting_key():
#     '''test setting non-existing key raises error'''
#     dicdot = DictDot({'foo': 5, 'bar': 'open'})
#     with pytest.raises(AttributeError):
#         setattr(dicdot, 'fred', 9)
def test_delete_nonexisting_key():
    '''test deleting nonexisting key'''
    ddot = DictDot()
    with pytest.raises(KeyError):
        del ddot['fred']
    with pytest.raises(AttributeError):
        del ddot.fred


def test_delete_existing_key():
    '''test deleting an existing key'''
    ddot = DictDot(foo=5)
    del ddot['foo']
    assert 'foo' not in ddot


def test_delete_existing_attr():
    '''test deleting an existing attr'''
    ddot = DictDot(foo=5)
    del ddot.foo
    assert 'foo' not in ddot
