"""test the math modules routines"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from decimal import Decimal as D
from math import log, factorial
import pytest

from dhp.math import MathError, UndefinedError
from dhp.math import fequal, is_even, is_odd, mean, gmean, pstddev, median
from dhp.math import sstddev, ttest_independent, hmean, mode
from dhp.math import log_nfactorial, prob_unique, choose


@pytest.mark.parametrize("num1,num2,expected", [
    (1, 2, False),
    (1, 1, True),
    (1.0, 1.00001, False),
    (1.0, 1.000001, True),
    (D('1'), D('1.000001'), False),
    (D('1'), D('1.0000001'), True),
    (1, 1.00001, False),            # i v. f
    (1, 1.000001, True),
    (1.00001, 1, False),            # f v. i
    (1.000001, 1, True),
    (1, D('1.000001'), False),      # i v. D
    (1, D('1.0000001'), True),
    ])
def test_fequal(num1, num2, expected):
    '''test default delta of 5 decimal places'''
    assert fequal(num1, num2) is expected


@pytest.mark.parametrize("num1,num2", [
    (1.0, D('1.000001')),    # f v. D
    (1.0, D('1.0000001')),
    ])
def test_fequal_typeerror(num1, num2):
    '''test type error is raised when types collide.'''
    with pytest.raises(TypeError):
        fequal(num1, num2)


def test_fequal_delta():
    '''test user set delta'''
    assert fequal(1.01, 1.02, delta=0.01) is False
    assert fequal(1.01, 1.02, delta=0.1)


@pytest.mark.parametrize("num,expected", [
    (20202, True),
    (30303, False),
    (0, True),
    (-2, True),
    (-1, False)
    ])
def test_is_even(num, expected):
    '''test is_even'''
    assert is_even(num) is expected


@pytest.mark.parametrize("num", [
    (1.0),
    (D('1')),
    ])
def test_iseven_not_int(num):
    '''test is_even against non Integer raises MathError'''
    with pytest.raises(MathError):
        is_even(num)


@pytest.mark.parametrize("num,expected", [
    (20202, False),
    (30303, True),
    (0, False),
    (-2, False),
    (-1, True)
    ])
def test_is_odd(num, expected):
    '''test is_odd'''
    assert is_odd(num) is expected


@pytest.mark.parametrize("num", [
    (1.0),
    (D('1')),
    ])
def test_isodd_not_int(num):
    '''test is_odd against non Integer raises MathError'''
    with pytest.raises(MathError):
        is_odd(num)


@pytest.mark.parametrize("nums,expected", [
    ([3, 9, 10, 8, 6, 5], 6.833333),
    ([3.0, 9.0, 10.0, 8.0, 6.0, 5.0], 6.833333),
    ([D('3'), D('9'), D('10'), D('8'), D('6'), D('5')], D('6.833333')),
    ])
def test_mean(nums, expected):
    """test mean routine"""
    assert fequal(mean(nums), expected)


@pytest.mark.parametrize("nums,expected", [
    ([3, 9.0, 10.0, 8.0, 6.0, 5.0], 6.833333),
    ([3, D('9'), D('10'), D('8'), D('6'), D('5')], D('6.833333')),
    ])
def test_mean_mixed(nums, expected):
    """test mean routine mixed float|Decimal and int"""
    assert fequal(mean(nums), expected)


@pytest.mark.parametrize("nums,etype", [
    ([D('3'), 9.0, 10.0, 8.0, 6.0, 5.0], TypeError),
    ([3.0, D('9'), D('10'), D('8'), D('6'), D('5')], TypeError),
    ([], UndefinedError),
    ])
def test_mean_exceptions(nums, etype):
    '''test mean of empty list raises MathError'''
    with pytest.raises(etype):
        mean(nums)


@pytest.mark.parametrize("nums,expected", [
    ([1, 3, 9, 27, 81], 9),
    ([2, 18], 6),
    ([3, 9, 10, 8, 6, 5], 6.337663371064305),
    ([3.0, 9.0, 10.0, 8.0, 6.0, 5.0], 6.337663371064305),
    ([D('3'), D('9'), D('10'), D('8'), D('6'), D('5')],
     D('6.337663371064305')),
    ])
def test_gmean(nums, expected):
    '''test gmean'''
    # assert fequal(gmean([1, 3, 9, 27, 81]), 9)
    # assert gmean([2, 18]) == 6
    assert fequal(gmean(nums), expected)


@pytest.mark.parametrize("nums,etype", [
    ([D('3'), 9.0, 10.0, 8.0, 6.0, 5.0], TypeError),
    ([3.0, D('9'), D('10'), D('8'), D('6'), D('5')], TypeError),
    ([], UndefinedError),
    ])
def test_gmean_exception(nums, etype):
    '''test gmean raising exceptions'''
    with pytest.raises(etype):
        gmean(nums)


@pytest.mark.parametrize("nums,expected", [
    ([3, 9, 10, 8, 6, 5], 5.79088471849866),
    ([3.0, 9.0, 10.0, 8.0, 6.0, 5.0], 5.79088471849866),
    ([D('3'), D('9'), D('10'), D('8'), D('6'), D('5')],
     D('5.79088471849866')),
    ([1, 2], 1.333333),
    ])
def test_hmean(nums, expected):
    '''test basic harmonic mean'''
    assert fequal(hmean(nums), expected)


@pytest.mark.parametrize("nums,etype", [
    ([D('3'), 9.0, 10.0, 8.0, 6.0, 5.0], TypeError),
    ([3.0, D('9'), D('10'), D('8'), D('6'), D('5')], TypeError),
    ([], UndefinedError),
    ([0], UndefinedError),
    ])
def test_hmean_exceptions(nums, etype):
    '''test harmonic mean throwing exceptions and empty list or 0'''
    with pytest.raises(etype):
        hmean(nums)


def test_pstddev():
    """test the pstddev routine"""
    lst = [1, 2, 3, 4, 5, 6]
    assert fequal(pstddev(lst), 1.707825)


def test_sstddev():
    '''test the sample stddev'''
    lst = [24, 7, 27, 12, 4]
    assert fequal(sstddev(lst), 10.2323017)


@pytest.mark.parametrize("nums,expected", [
    ([3, 13, 7, 5, 21, 23, 39, 23, 40, 23, 14, 12, 56, 23, 29], 23),  # odd N
    ([3, 13, 7, 5, 21, 23, 23, 40, 23, 14, 12, 56, 23, 29], 22),      # even N
    ([1], 1),
    ([1, 1], 1),
    ([1, 1, 2, 4], 1.5),
    ([0, 2, 5, 6, 8, 9, 9], 6),
    ([0, 0, 0, 0, 4, 4, 6, 8], 2),
    ([3, 9, 10, 8, 6, 5], 7.0),
    ([3.0, 9.0, 10.0, 8.0, 6.0, 5.0], 7.0),
    ([D('3'), D('9'), D('10'), D('8'), D('6'), D('5')], D(7)),
    ([1, 2], 1.5),
    ([1.0, 2], 1.5),
    ([D(1), 2], 1.5),
    ])
def test_median(nums, expected):
    '''test the median function'''
    assert median(nums) == expected


@pytest.mark.parametrize("nums,etype", [
    ([D('3'), 9.0], TypeError),
    ([7.0, D('9')], TypeError),
    ([], UndefinedError),
    ])
def test_median_exceptions(nums, etype):
    '''Test median raising exceptions.'''
    with pytest.raises(etype):
        median(nums)


def test_mode():
    '''test mode'''
    assert mode([1, 3, 3, 5]) == 3
    with pytest.raises(MathError):
        mode([1, 3, 3, 5, 5])
    with pytest.raises(MathError):
        mode([])


def test_ttest_ind():
    '''test the ttest for independent samples'''
    lst1 = [1, 2, 3, 4, 5, 6]
    lst2 = [2, 2, 3, 4, 5, 6]
    assert fequal(abs(ttest_independent(lst1, lst2)), .1643989)


@pytest.mark.parametrize("nvals, ssize, prob", [
    (3, 1, 1),
    (13, 5, 0.4159518224151815),
    (365, 23, 0.4927027656760146)
])
def test_prob_unique(nvals, ssize, prob):
    """Test probability of uniqueness"""
    assert prob_unique(nvals, ssize) == prob


@pytest.mark.parametrize("n", range(9, 210))
def test_log_nfactorial(n):     # pylint:disable=c0103
    """test the approximation prowess of log_nfactorial."""
    assert fequal(log(factorial(n)), log_nfactorial(n))


@pytest.mark.parametrize("n, k, c", [
    (4, 2, 6),
    (1, 2, 0),
    (3, 2, 3),
    (52, 5, 2598960)
])      # pylint:disable=c0103
def test_choose(n, k, c):
    """Test the n Choose k function."""
    assert choose(n, k) == c
