dhp.transforms
==============

.. _to-snake-link:

to_snake
--------
given a "camelCase" string, transform it into a python-esque "camel_case".

.. function:: to_snake(name)

   return pythonized format of name, assumes name is some camelCase variant.

   :param name: camel cased named to transform
   :rtype: a pythonized string representation of the camel cased name.

.. code:: python

    from dhp.transforms import to_snake

    assert to_snake('camelCase') == 'camel_case'


**Use case**: helpful when converting awful xml that uses camelCase to a python representation.
