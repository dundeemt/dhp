.. dhp documentation master file, created by
   sphinx-quickstart on Fri May  2 13:10:16 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to dhp's documentation!
*******************************

Dirty Hungarian Phrasebook
==========================
dhp is a library of snippets, almost guaranteed to get you into trouble.

I obtained it, from a vendor, on the corner, outside of PyCon.


Actually, this is a growing repository of routines that I find helpful from time to time. I think you might too.

Phrasebook Examples
-------------------

:doc:`dhp-doq` -- Use ORM like expressions to query simple data sources.

:doc:`dhp-search` -- Search related method and functions.

* :any:`fuzzy-search-link` - search like "Sublime Text"


:doc:`dhp-structures` -- Unique structures that build on Python's built-ins.

 * **DictDot** - Ever wish the dictionary supported dot access?


:doc:`dhp-test` -- Helpful test helper routines.

* :any:`tempfile-containing-link` - generate a temporary file that contains indicated contents and returns the filename for use.  When finished the tempfile is removed.


:doc:`dhp-transforms`

* :any:`to-snake-link` - transform a "camelCased" name into a pythonized version, "camel_cased".


:doc:`dhp-vi`

* :any:`iteritems-link` - return the proper iteritems method for a dictionary based on the version of Python


:doc:`dhp-xml`

* :any:`xml-to-dict-link` - parse any ugly, but valid, xml to a python dictionary.
* :any:`ppxmllink` - format/reformat any ugly but valid xml, a pretty printer for xml



Supports
--------
Tested on Python 2.7, 3.2, 3.3, 3.4

.. image:: https://drone.io/bitbucket.org/dundeemt/dhp/status.png
    :target: https://drone.io/bitbucket.org/dundeemt/dhp/latest
    :alt: Build Status

Requirements
------------
None.

Installation
------------
Make sure to get the latest version.

  pip install dhp

Download
--------
* https://pypi.python.org/pypi/dhp

Project Site
------------
* https://bitbucket.org/dundeemt/dhp

License
-------
BSD

Documentation
--------------
* http://dhp.rtfd.org/

Change Log
----------
See :doc:`changelog`

Contributing
------------
See :doc:`contributing`

Contributors
------------
See :doc:`contributors`

Contents:

.. toctree::
   :maxdepth: 2

   changelog
   contributing
   contributors
   dhp-doq
   dhp-math
   dhp-search
   dhp-structures
   dhp-tempus
   dhp-test
   dhp-transforms
   dhp-xml
   dhp-vi
   release
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

