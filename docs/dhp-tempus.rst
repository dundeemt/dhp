
dhp.tempus
==========
This module includes tools to deal with time, dates, and intervals.

delta_from_interval
-------------------
return a python datetime.timedelta that is repesented by an human parseable Interval string.
NwNdNhNmNs, i.e. 1w2d3h4m5s - One week, 2 days, 3 hours, 4 minutes and 5 seconds.  Which can 
be quite useful if you want a human to schedule a delay or time based repeat interval.

.. function:: delta_from_interval(interval)

   return a python datetime.timedelta represented by interval.

   :param interval: str
   :rtype: datetime.timedelta

.. code:: python

    from dhp.tempus import delta_from_interval

    for k, val in iteritems(my_dict):
        do_something(k, val)

**Use case**:  supporting python2 code that uses iteritems when targeting both 2 and 3.

PY_VER
-------
is set to the major version of python currently running. Either 2 or 3 respectively.


StringIO
--------
Imports the correct StringIO for the currently running version of Python.

.. code:: python

    from dhp.VI import StringIO
