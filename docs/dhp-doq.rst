dhp.doq
=======

DOQ
---
pronounced `Duke` allows you to query an list, iterable or generator of objects
with a Django ORM like / Fluent interface.  This is useful for exploratory
programming and also it is just a nice, comfortable inteface to query your
data objects. DOQ supports lazy evaluations and nested objects.


Example
-------
Say you had a csv file of employee records and you wanted to list the
employees in the IT department.  Well you could do the traditional thing or ...


.. code:: python

    EmployeeRecord = namedtuple('EmployeeRecord', 'emp_id, name, dept, hired')


    def csvtuples():
        '''csv named tuple emitter.'''
        reader = csv.reader(TEST_FILE)
        for emp in map(EmployeeRecord._make, reader):
            yield emp

    doq = DOQ(data_objects=csvtuples())
    for emp in doq.filter(dept='IT'):
        print(emp)

    # Now let's list everyone who is not in IT.
    for emp in doq.exclude(dept='IT'):
        print(emp)

    # ok, now let's sort the not IT employees by name
    for emp in doq.exclude(dept='IT').order_by('name'):
        print(emp)

Yes, it is just that easy. You can chain .filter() and .exclude(). There is a
.get method that raises DoesNotExist and MultipleObjectsReturned.  All that
ooohey gooey goodness of an full blown ORM but quick and easy and works
without a lot of setup.

Let's throw some remote json data at the Duke and see what happens.

.. code:: python

    from dhp.structures import DictDot
    from dhp.doq import DOQ
    import requests

    def json_ds(url):
        # fetch some json data, transform the returned dict to DictDot so
        # we can access attributes with dotted notation and then return
        # a DOQ with that data.
        data_objects = [DictDot(x) for x in requests.get(url).json()]
        return DOQ(data_objects=data_objects)

    users = json_ds('http://jsonplaceholder.typicode.com/users')
    type(users)         # prints  <class 'dhp.doq.DOQ'>
    users.all().count   # prints 10
    user = users.all()[0]
    type(user)       # prints  <class 'dhp.structures.DictDot'>
    user.id          # prints  1
    user.address.suite  # prints  u'Apt. 556'
    users.filter(address__suite__startswith='Apt.').count     # prints 3



One quick note before we head into the full documenation. DOQ is NOT a full
blown Object Relation Manager. It does not create databases, nor know how to
access them. If that is what you desire, then SQLAlchemy, Pony, PeeWeeDB or
Django's ORM is probably going to get you what you want.

If you are looking to slap some lipstick on a simple data source, well then,
DOQ is your color.  :doc:`dhp.doq` for api specifics.


