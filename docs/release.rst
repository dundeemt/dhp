Release Procedures
==================
Notes on how to prepare, package and release a new version


Pre-Release
-----------
1. You should have the packages in `requirements-dev.txt` installed and run
   setup.

.. code:: bash

    pip install -U -r requirements-dev.txt
    pip install -e .

2. Code should be checked in

.. code:: bash

    hg sum --remote

3. Tests should be passing locally

.. code:: bash

    py.test -v

4. drone and AppVeyor tests should be passing 
   
   * https://drone.io/bitbucket.org/dundeemt/dhp/latest
   * https://ci.appveyor.com/project/dundeemt91221/dhp

5. Update the changelog

6. Read the Docs builds should be building cleanly -- http://dhp.readthedocs.org/en/latest/

7. Run the release script in --dry-run mode and check that no errors or issues are outstanding. Specifically, check version information from bumpversion.

.. code:: bash

    ./release.sh --dry-run


Release
-------
bumping the version, checking the build, committing tags

1. finalize the changelog (changlog.rst)
   
2. Run the release script

.. code:: bash

    ./release.sh

3. Push the commit

.. code:: bash

    hg push


3. Verify drone builds -- https://drone.io/bitbucket.org/dundeemt/dhp/latest

4. Verify the Appveyor build --
   https://ci.appveyor.com/project/dundeemt/dhp-dev

5. Verify docs built -- http://dhp.readthedocs.org/en/latest/

6. Set the default docs to the new version -- https://readthedocs.org/dashboard/dhp/versions/

7. upload to pypi

.. code:: bash

    twine upload dist/dhp-x.y.z.tar.gz


8. InsecurePlatformWarning  - If you get this warning on python2.7+ you will need to install some additional modules

.. code:: bash

    pip install pyopenssl ndg-httpsclient pyasn1

9. Check PyPi for problems and make sure docs and package is correct -- https://pypi.python.org/pypi/dhp


Profit
------
You and the rest of the world can enjoy
