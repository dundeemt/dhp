Contributing
=============
Notes on how to contribute


Setting up a dev environment
----------------------------
These instructions assume you are developing in a virtualenv, you are, aren't  you?

1. Clone the code into your virtualenv

2. You should have the packages in `dev-requirements.txt` installed

.. code:: bash

    pip install -r requirements-dev.txt

3. install dhp as editable

.. code:: bash

    pip install -e .

4. Tests should be passing locally

.. code:: bash

    py.test

5. Editing documentation - you will need to build the docs initially then use docwatch, to auto build the docs when saved as you edit.

.. code:: bash

    cd docs
    make html
    cd ..

.. code:: bash

    python docwatch.py


Pull Requests
-------------

* Code should be passing all tests locally, bonus points for passing drone.io
* New code should have new tests to go along with it.
* Code should be pep8 compliant
* update documentation as necessary
* update contributors.rst
* make a pull request
