dhp.VI package
==============

Module contents
---------------

.. automodule:: dhp.VI
    :members:
    :undoc-members:
    :show-inheritance:

Exports
-------
The following are exported by dhp.VI

StringIO
^^^^^^^^
The proper version of StringIO from cStringIO or io package. ::

    from dhp.VI import StringIO


InstanceType
^^^^^^^^^^^^
Python3 does not have old-style classes so types.InstanceType is undefined. If
python3 then InstanceType is set as a pointer to object -- which is probably
not what you want. ::

    from dhp.VI import InstanceType


