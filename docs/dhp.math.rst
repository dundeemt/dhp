dhp.math package
================

Module contents
---------------

.. automodule:: dhp.math
    :members:
    :undoc-members:
    :show-inheritance:
