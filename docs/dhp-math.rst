dhp.math
========

.. _fequal-link:

fequal
------
compare to floats to see if they are equal within a tolerance

.. function:: fequal(num1, num2, tolerance=0.000001)

   return True if num1 is within tolerance of num2, else false

   :param num1: float
   :param num2: float
   :param tolerance: float
   :rtype: boolean

.. code:: python

    from dhp.math import fequal

    assert fequal(1.123456, 1.1234561)


**Use case**: comparing floats can be interesting due to internal representations

is_even
-------
returns True if integer is even

.. function:: is_even(num)

    :param num: int
    :rtype: boolean

is_odd
------
returns True if integer is odd

.. function:: is_odd(num)

    :param num: int
    :rtype: boolean

mean
----
returns the Arithmetic mean (a/k/a average) of a list of numbers

.. function:: mean(lst)

    :param list: float | int | mixed
    :rtype: float

gmean
-----
returns the Geometric mean of a list of numbers

.. function:: gmean(lst)

    :param list: float | int | mixed
    :rtype: float

hmean
-----
returns the Harmonic mean of a list of numbers

.. function:: hmean(lst)

    :param list: float | int | mixed
    :rtype: float
