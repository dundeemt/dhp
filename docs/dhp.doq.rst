dhp.doq package
===============

Module contents
---------------

.. automodule:: dhp.doq
    :members:
    :undoc-members:
    :show-inheritance:

.. _attribute-lookups:

Attribute Lookups
-----------------
Attribute lookups are similar to how you specify the meat of an SQL WHERE
clause. They’re specified as keyword arguments to the DOQ methods
:func:`~dhp.doq.DOQ.filter`, :func:`~dhp.doq.DOQ.exclude` and
:func:`~dhp.doq.DOQ.get`.

The format of look_ups is attribute_name__operation=value  That is the name
of the attribute to look at, a double under score(dunder) and then the lookup
operator, an equals sign and then the value to compare against.  The format
was inspired by Django's ORM.

DOQ’s inbuilt lookups are listed below.

As a convenience when no lookup type is provided (like in ``doq.get(emp_id=14)``)
the lookup type is assumed to be `exact`_.

exact
^^^^^
Exact case-sensitive match. ::

    doq.get(emp_id__exact=4)
    assert doq.get(name='Jeff') == doq.get(name__exact='Jeff')

iexact
^^^^^^
Exact, case insensitive, match. ::

    doq.filter(name__iexact='jeff')  # would match, jEFF, Jeff, etc.

lt
^^
Less Than. ::

    doq.filter(emp_id__lt=3)        # given [4, 3, 2, 1], would match [2, 1]

lte
^^^
Less Than or Equal to. ::

    doq.filter(emp_id__lte=3)        # given [4, 3, 2, 1], would match [3, 2, 1]

gt
^^
Greater Than. ::

    doq.filter(emp_id__gt=3)        # given [4, 3, 2, 1], would match [4, ]


gte
^^^
Greater Than or Equal To. ::

    doq.filter(emp_id__gte=3)        # given [4, 3, 2, 1], would match [4, 3]


contains
^^^^^^^^
If the value is in the attribute. ::

    doq.filter(name__contains='o') # given ['Oscar', 'John', 'Jo'], would match ['John', 'Joe']

icontains
^^^^^^^^^
Case insensitive version of contains. See above. ::

    doq.filter(name__icontains='o') # given ['Oscar', 'John', 'Jo'], would match ['Oscar', John', 'Joe']

startswith
^^^^^^^^^^
If the attribute value startswith. ::

    doq.filter(name__startswith='O') # given ['Oscar', 'John', 'Jo'], would match ['Oscar', ]

istartswith
^^^^^^^^^^^
Case insensitive version of startswith. See above. ::

    doq.filter(name__istartswith='o') # given ['Oscar', 'John', 'Jo'], would match ['Oscar', ]

endswith
^^^^^^^^
If the attribute value endswith. ::

    doq.filter(name__endswith='n') # given ['Oscar', 'John', 'Jo'], would match ['John', ]

iendswith
^^^^^^^^^
Case insensitive version of endswith. See above. ::

    doq.filter(name__iendswith='N') # given ['Oscar', 'John', 'Jo'], would match ['John', ]

in
^^
If the attribute value is in the list supplied. ::

    doq.filter(emp_id__in=[1, 3])   # given [1, 2, 3, 4], would match [1, 3]

range
^^^^^
Is a short hand equivalent of a >= b and a <= c where `a__range=(b, c)` and b <= c ::

    doq.filter(emp_id__range=(2, 5))  # is equivalent of doq.filter(emp_id__gte=2, emp_id__lte=5)


Nested Objects
--------------
If you have a object that is composed of nested objects, you can access the
values of the nested subobjects by using double underscores to list the path
of the relationship.  Say you had a list of objects with the following layout:

::

    user:
        id
        name
        address:
            street
            suite
            zipcode
            geo:
                lat
                lon

You would access the top-level attributes. ::

    doq.filter(id=7)`

To access the suite information, ::

    doq.filter(address__suite='Apt. 201')

which would be an exact match on the attribute value.  To use another operator
with your lookup just specify it. ::

    doq.filter(address__suite__startswith='Apt.')

Ordering on a nested attribute is the same. To order by lat::

    doq.all().order_by('address__geo__lat')


Slicing DOQ (Limiting)
----------------------
Slicing a DOQ is supported.  Since we are not performing SQL the results of a
slicing operation are immediate and return a list of data_objects.

::

    >>> type(doq.all()[2:4])
    <type 'list'>

This also means that Negative indexing is supported.

::

    doq.all()[-1]

Would return the last data_object from the results.
