dhp.xml
=======

.. _xml-to-dict-link:

xml_to_dict
-----------
There are a number of examples, on the intertubes, of doing this exact thing.  However, many of them die on attributes.  This has proven to be a robust routine and has dealt with all valid xml thrown at it.

.. function:: xml_to_dict(xml)

   convert valid xml to a python dictionary

   :param xml: string containing xml to be converted
   :rtype: dictionary

.. code:: python

    from dhp.xml import xml_to_dict

    xml = '<vehicle type="Hovercraft"><filled/><cargo>eels</cargo></vehicle>'
    xml_to_dict(xml)

    {'vehicle': {'@type':'Hovercraft',
                 'cargo':'eels',
                 'filled': None}
    }

**Use case**: parse any ugly, but valid, xml to a python dictionary.


.. _ppxmllink:

ppxml
-----
Pretty print xml. reformat xml in a sane way.  Often times xml from external/3rd party sources is delivered like a gigantic furball, making it hard for a human to parse/read, this utility function makes it a bit more palatable.

.. function:: ppxml(xml)

   format xml for easier viewing

   :param xml: string containing xml to be formatted
   :rtype: string

.. code:: python

  >>> from dhp.xml import ppxml
  >>> xml = '<vehicle type="Hovercraft"><filled/><cargo>eels</cargo></vehicle>'
  >>> ppxml(xml)
  u'<?xml version="1.0" ?>\n<vehicle type="Hovercraft">\n  <filled/>\n  <cargo>eels</cargo>\n</vehicle>\n'
  >>> print ppxml(xml)
  <?xml version="1.0" ?>
  <vehicle type="Hovercraft">
    <filled/>
    <cargo>eels</cargo>
  </vehicle>
