dhp.VI
======
These are simple methods for dealing with Python 2/3 compatibility issues.  They are focused on solving the problems of python 2/3 support in the dhp package.  If you need more see `six <https://pypi.python.org/pypi/six>`_

.. _iteritems-link:

iteritems
---------
return the proper iteritems method for a dictionary based on the version of Python

.. function:: iteritems(dct)

   return proper iteritems method

   :param dct: dictionary
   :rtype: iterable method

.. code:: python

    from dhp.VI import iteritems

    for k, val in iteritems(my_dict):
        do_something(k, val)

**Use case**:  supporting python2 code that uses iteritems when targeting both 2 and 3.

PY_VER
-------
is set to the major version of python currently running. Either 2 or 3 respectively.


StringIO
--------
Imports the correct StringIO for the currently running version of Python.

.. code:: python

    from dhp.VI import StringIO
