dhp.search
==========

.. _fuzzy-search-link:

fuzzy_search
------------
given a list of strings(haystack) to search, return those elements, ranked, that fuzzily match the search term(needle).

.. function:: fuzzy_search(needle, haystack)

   return a ranked list of elements from haystack that fuzzily match needle

   :param needle: what you are searching to find
   :param haystack: list of things to search
   :rtype: ranked sublist of haystack elements matching needle

.. code:: python

    from dhp.search import fuzzy_search

    haystack = ['.bob', 'bob.', 'bo.b', 'fred']
    assert fuzzy_search(needle='bob', haystack) == ['bob.', '.bob', 'bo.b']


**Use case**: create a "Sublime Text" like search experience
