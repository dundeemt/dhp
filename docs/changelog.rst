Change Log
==========

0.0.15 (dev)
----------------------------
 * dhp.math.log_nfactorial - using Ramanujan's approximation
 * dhp.math.prob_unique - probability of no collision
 * dhp.math.choose - nCk
 * dhp.VI.to_unicode - helper for dealing with unicode in Py2
 * dhp.VI.set_output_encoding - helper for dealing with unicode in Py2
 
0.0.14 (released 2016-02-05)
----------------------------
 * added dhp.structures.ComparableMixin to aid in creating classes with rich
   comparisons.
 * dropped support for Python3.2

0.0.13 (released 2015-12-20)
----------------------------
 * integration with Appveyor CI for windows testing
 * :doc:`dhp.tempus` - humane time interval transforms

   * dhp.tempus.interval_from_delta - transform a datetime.timedelta to an
     interval.
   * dhp.tempus.delta_from_interval - transform an interval string to a
     datetime.timedelta object. 

 * :doc:`dhp.cache` - a simple cache class

0.0.12 (released 2015-09-27)
----------------------------
 * dhp.doq.DOQ - implemented range operator for lookups
 * dhp.VI - StringIO - export StringIO from the proper package based on py2/py3
 * dhp.search - Improved documentation.
 * dhp.math - Improved documentation. Improved type tolerance. int/float/decimal
 * test coverage improved on all submodule that were less than 100%

0.0.11 (released 2015-09-23)
----------------------------
 * dhp.doq.DOQ - Duke is on the job to handle all your simple data source querying needs.

0.0.10 (released 2015-09-22)
----------------------------
 * dhp.structures.DictDot - initial implementation

0.0.9 (released 2015-08-23)
---------------------------
 * dhp.search.fuzzy_search - made case insensitive

0.0.8 (released 2015-08-19)
---------------------------
 * refactor of test suite now that we are using `pip install -e .`

0.0.7 (released 2015-06-27)
---------------------------
 * dhp.search.fuzzy_search and .fuzzy_distance
